import copy
import math
import torch
from torch import nn
import torch.nn.functional as F
import numpy as np

# 克隆一个模块N次
def clones(module, N):
    return nn.ModuleList([copy.deepcopy(module) for _ in range(N)])

# 用于掩盖样本数据中后续时间步的函数
def subsequent_mask(size):
    attn_shape = (1, size, size)
    subsequent_mask = np.triu(np.ones(attn_shape), k=1).astype('uint8')
    return torch.from_numpy(subsequent_mask) == 0

class EncoderDecoder(nn.Module):
    """
    一个标准的编码器-解码器架构。这是该模型和许多其他模型的基础。
    """
    def __init__(self, encoder, decoder, src_embed, tgt_embed, generator):
        super(EncoderDecoder, self).__init__()
        self.encoder = encoder  # 编码器
        self.decoder = decoder  # 解码器
        self.src_embed = src_embed  # 源数据嵌入
        self.tgt_embed = tgt_embed  # 目标数据嵌入
        self.generator = generator  # 生成器
        
    def forward(self, src, tgt, src_mask, tgt_mask):
        "处理带掩码的源和目标序列。"
        return self.decode(self.encode(src, src_mask), src_mask,
                           tgt, tgt_mask)
    
    def encode(self, src, src_mask):
        return self.encoder(self.src_embed(src), src_mask)
    
    def decode(self, memory, src_mask, tgt, tgt_mask):
        return self.decoder(self.tgt_embed(tgt), memory, src_mask, tgt_mask)

class Generator(nn.Module):
    "定义标准的线性+softmax生成步骤。"
    def __init__(self, d_model, vocab):
        super(Generator, self).__init__()
        self.proj = nn.Linear(d_model, vocab)

    def forward(self, x):
        return F.log_softmax(self.proj(x), dim=-1)

# 实现LayerNorm（Pytorch也有LayerNorm）
class LayerNorm(nn.Module):
    "构建一个LayerNorm模块（参见引用的详细信息）。"
    def __init__(self, features, eps=1e-6):
        super(LayerNorm, self).__init__()
        # 声明需要学习的参数
        self.a_2 = nn.Parameter(torch.ones(features))
        self.b_2 = nn.Parameter(torch.zeros(features))
        self.eps = eps

    def forward(self, x):
        mean = x.mean(-1, keepdim=True)
        std = x.std(-1, keepdim=True)
        return self.a_2 * (x - mean) / (std + self.eps) + self.b_2

# 实现注意力机制（缩放点积注意力）
def attention(query, key, value, dropout=None):
    d_k = query.size(-1)
    key = key.transpose(-2, -1)
    scores = torch.matmul(query, key) / math.sqrt(d_k)
    p_attn = F.softmax(scores, dim=-1)
    if dropout is not None:
        p_attn = dropout(p_attn)
    attention_result = torch.matmul(p_attn, value)
    return attention_result, p_attn

class MultiHeadedAttention(nn.Module):
    def __init__(self, h, hidden_size, linears=True, dropout=0.1):
        super(MultiHeadedAttention, self).__init__()
        assert hidden_size % h == 0
        # 我们假设d_v总是等于d_k
        self.d_k = hidden_size // h
        self.h = h
        if linears: 
            self.linears = clones(nn.Linear(hidden_size, hidden_size), 4)
        else:
            self.linears = [lambda arg: arg] * 4
        self.attn = None
        self.dropout = nn.Dropout(p=dropout)
        
    def forward(self, query, key, value):
        nbatches = query.size(0)
        
        # 1) 在批处理中从hidden_size => h x d_k进行所有线性投影
        query, key, value = [l(x).view(nbatches, -1, self.h, self.d_k).transpose(1, 2) for l, x in zip(self.linears, (query, key, value))]
        
        # 2) 在批处理中对所有投影向量应用注意力机制
        x, self.attn = attention(query, key, value, self.dropout)
        
        # 3) 使用view进行“连接”并应用最终的线性变换
        x = x.transpose(1, 2).contiguous().view(nbatches, -1, self.h * self.d_k)
        
        # 从最后一个线性层获取结果
        x = self.linears[-1](x)
        return x

class Batch:
    "用于在训练过程中保存批数据和掩码的对象。"
    def __init__(self, src, trg=None, pad=0):
        self.src = src
        self.src_mask = (src != pad).unsqueeze(-2)
        if trg is not None:
            self.trg = trg[:, :-1]
            self.trg_y = trg[:, 1:]
            self.trg_mask = self.make_std_mask(self.trg, pad)
            self.ntokens = (self.trg_y != pad).data.sum()
    
    @staticmethod
    def make_std_mask(tgt, pad):
        "创建一个掩码来隐藏填充和未来的单词。"
        tgt_mask = (tgt != pad).unsqueeze(-2)
        # Had Variable
        tgt_mask = tgt_mask & subsequent_mask(tgt.size(-1)).type_as(tgt_mask.data)
        return tgt_mask

def make_model(src_vocab, tgt_vocab, N=6, 
               d_model=512, d_ff=2048, h=8, dropout=0.1):
    "辅助函数：根据超参数构建模型。"
    c = copy.deepcopy
    attn = MultiHeadedAttention(h, d_model)
    ff = PositionwiseFeedForward(d_model, d_ff, dropout)
    position = PositionalEncoding(d_model, dropout)
    model = EncoderDecoder(
        Encoder(EncoderLayer(d_model, c(attn), c(ff), dropout), N),
        Decoder(DecoderLayer(d_model, c(attn), c(attn), 
                             c(ff), dropout), N),
        nn.Sequential(Embeddings(d_model, src_vocab), c(position)),
        nn.Sequential(Embeddings(d_model, tgt_vocab), c(position)),
        Generator(d_model, tgt_vocab))
    
    # 这在他们的代码中很重要。
    # 使用Glorot / fan_avg初始化参数。
    for p in model.parameters():
        if p.dim() > 1:
            nn.init.xavier_uniform_(p)
    return model

# 生成随机数据，其中源和目标相同
# 例如：batch = next(iter(data_gen(5, 4, 20)))
def data_gen(V, batch, nbatches):
    for i in range(nbatches):
        data = torch.from_numpy(np.random.randint(1, V, size=(batch, 10)))
        data[:, 0] = 1
        src = data
        tgt = data
        
        # yield是python中生成器的返回
        yield Batch(src, tgt, 0)

class LabelSmoothing(nn.Module):
    "实现标签平滑。"
    def __init__(self, size, padding_idx, smoothing=0.0):
        super(LabelSmoothing, self).__init__()
        self.criterion = nn.KLDivLoss(reduction='sum')
        self.padding_idx = padding_idx
        self.confidence = 1.0 - smoothing
        self.smoothing = smoothing
        self.size = size
        self.true_dist = None

    def forward(self, x, target):
        assert x.size(1) == self.size
        true_dist = x.data.clone()
        true_dist.fill_(self.smoothing / (self.size - 2))
        target = target.long()  # 确保目标是int64类型
        true_dist.scatter_(1, target.data.unsqueeze(1), self.confidence)
        true_dist[:, self.padding_idx] = 0
        mask = torch.nonzero(target.data == self.padding_idx, as_tuple=False)
        if mask.dim() > 0:
            true_dist.index_fill_(0, mask.squeeze(), 0.0)
        self.true_dist = true_dist
        return self.criterion(x, true_dist)
