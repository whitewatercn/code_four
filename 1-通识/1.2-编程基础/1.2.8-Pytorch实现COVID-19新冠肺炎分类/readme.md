## 该项目分为基础版和进阶版，同学们了解基础版即可，进阶版作为一个扩充。

**项目数据集链接：** [点击这里](https://pan.baidu.com/s/1W9vE8H1KHFuq01Rmcw3ELw?pwd=DTAI)

权重可以自己训练生成，这里给出基础版的权重链接：[点击这里](https://pan.baidu.com/s/1AVyi5lQC4QgA5zmwZMHhVg?pwd=DTAI)。

**基础版：**
- COVID_19_Dataset_conversion.ipynb（数据集的转换）
- train_simple_gpu（train_simple_cpu即是将数据和模型在cpu下训练）
- val.ipynb（利用train_simple_gpu训练生成的权重进行推理并可视化）

*model_pth* 里面包含的是训练生成的权重文件。

### 运行项目前需要先搭建好项目环境
- 安装PyTorch深度学习框架(第一节环境配置有讲)
- 安装tqdm、tensorboardX包(终端输入pip install 包名)

**项目运行方式：**
1. 先将数据集下载，放到项目文件夹内，与视频内容保持一致
2. 运行 *COVID_19_Dataset_conversion.ipynb* 数据集转换完成，运行此文件会生成新的数据集文件夹：new_COVID_19_Radiography_Dataset。
3. 运行 *train_simple_gpu* 模型训练，训练完成后在model_pth下会生成best.pth(权重文件)。
4. 运行 *val.ipynb* 可视化查看，将权重加载进模型后进行单张图像的预测，输入一张图像推出属于哪个类别。

**进阶版：**
- train.py（可以采用一些用于分类的网络进行训练，比如 resnet18）
- utils.py（里面有一些用于辅助的函数，比如计算准确度等）
- test.ipynb（用测试集查看训练后的模型精确度如何，并给出推理可视化）
- Getstatistics.ipynb（获取均值和标准差，可以不用这个）

**需要先运行train.py生成report和run文件夹**
*report* 下面为我们训练得到的权重文件和训练记录，*run* 下面为可以使用 tensorboard 可视化的文件（使用方式为在终端输入 tensorboard --logdir=runs\new_COVID_19_Radiography_Dataset\resnet18\seed33，logdir=后面的为文件夹，里面含有一个 events 即可），*models* 里面为一些网络，比如：resnet、mobilenet等。

**项目运行方式：**
1. 运行 *COVID_19_Dataset_conversion.ipynb* 数据集转换完成。
2. 运行 *train.py* 训练，会生成对应的report和run文件夹下的内容，里面包含权重文件以及loss记录等。
3. 运行 *test.ipynb* 推理并可视化，计算模型在测试集上的精确度，并输入一张图像可以推理出属于哪种类别。
