import torch
import torchvision
import matplotlib.pyplot as plt

# 定义数据预处理
transform = torchvision.transforms.Compose([
    torchvision.transforms.ToTensor(),
])

# 加载FashionMNIST数据集
train_dataset = torchvision.datasets.FashionMNIST(root='./data', train=True, download=True, transform=transform)

# 创建数据加载器
train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=10, shuffle=True)

# 可视化部分图像
plt.figure(figsize=(10, 5))
for images, labels in train_loader:  # 遍历数据加载器，每次迭代获取一个批次的图像和标签
    print(images.shape)
    # print(labels.shape)
    for i in range(10):  # 遍历每个批次中的样本
        plt.subplot(2, 5, i+1)
        plt.imshow(images[i].squeeze(), cmap='gray')
        plt.title(f'Label: {labels[i].item()}')
        plt.axis('off')
    plt.show()
    # 只展示一个批次的图像，这里使用 break 结束循环
    break