import torch as t
from torch import nn
from torch.autograd import Variable
from torch.optim import Adam
from torchvision import transforms
from torchvision.utils import make_grid
from torchvision.datasets import CIFAR10, MNIST
from pylab import plt 
import os

# 创建保存结果图像的文件夹
result_dir = 'results'
os.makedirs(result_dir, exist_ok=True)

class Config:
    lr = 0.0002
    nz = 100 # noise dimension
    image_size = 64
    image_size2 = 64
    nc = 1 # chanel of img 
    ngf = 64 # generate channel
    ndf = 64 # discriminative channel
    beta1 = 0.5
    batch_size = 32
    max_epoch = 10 # =1 when debug
    workers = 2
    gpu = True # use gpu or not
    
opt=Config()

# data preprocess
transform=transforms.Compose([
                transforms.Resize(opt.image_size),
                transforms.ToTensor(),
                transforms.Normalize([0.5], [0.5])
                ])

dataset=MNIST(root='data', transform=transform, download=True)
# dataloader with multiprocessing
dataloader=t.utils.data.DataLoader(dataset,
                                   opt.batch_size,
                                   shuffle=True,
                                   num_workers=opt.workers)

# define model
class Generator(nn.Module):
    def __init__(self):
        super(Generator, self).__init__()
        self.main = nn.Sequential(
            nn.ConvTranspose2d(opt.nz,opt.ngf*8,4,1,0,bias=False),
            nn.BatchNorm2d(opt.ngf*8),
            nn.ReLU(True),
            nn.ConvTranspose2d(opt.ngf*8,opt.ngf*4,4,2,1,bias=False),
            nn.BatchNorm2d(opt.ngf*4),
            nn.ReLU(True),
            nn.ConvTranspose2d(opt.ngf*4,opt.ngf*2,4,2,1,bias=False),
            nn.BatchNorm2d(opt.ngf*2),
            nn.ReLU(True),
            nn.ConvTranspose2d(opt.ngf*2,opt.ngf,4,2,1,bias=False),
            nn.BatchNorm2d(opt.ngf),
            nn.ReLU(True),
            nn.ConvTranspose2d(opt.ngf,opt.nc,4,2,1,bias=False),
            nn.Tanh()
        )

    def forward(self, input):
        return self.main(input)

class Discriminator(nn.Module):
    def __init__(self):
        super(Discriminator, self).__init__()
        self.main = nn.Sequential(
            nn.Conv2d(opt.nc,opt.ndf,4,2,1,bias=False),
            nn.LeakyReLU(0.2,inplace=True),
            nn.Conv2d(opt.ndf,opt.ndf*2,4,2,1,bias=False),
            nn.BatchNorm2d(opt.ndf*2),
            nn.LeakyReLU(0.2,inplace=True),
            nn.Conv2d(opt.ndf*2,opt.ndf*4,4,2,1,bias=False),
            nn.BatchNorm2d(opt.ndf*4),
            nn.LeakyReLU(0.2,inplace=True),
            nn.Conv2d(opt.ndf*4,opt.ndf*8,4,2,1,bias=False),
            nn.BatchNorm2d(opt.ndf*8),
            nn.LeakyReLU(0.2,inplace=True),
            nn.Conv2d(opt.ndf*8,1,4,1,0,bias=False),
            nn.Sigmoid()
        )
        self.feature_extractor = nn.Sequential(
            nn.Conv2d(opt.nc,opt.ndf,4,2,1,bias=False),
            nn.LeakyReLU(0.2,inplace=True),
            nn.Conv2d(opt.ndf,opt.ndf*2,4,2,1,bias=False),
            nn.BatchNorm2d(opt.ndf*2),
            nn.LeakyReLU(0.2,inplace=True),
            nn.Conv2d(opt.ndf*2,opt.ndf*4,4,2,1,bias=False),
            nn.BatchNorm2d(opt.ndf*4),
            nn.LeakyReLU(0.2,inplace=True)
        )

    def forward(self, input):
        return self.main(input)

    def extract_features(self, input):
        return self.feature_extractor(input)

# Initialize generator and discriminator
netg = Generator()
netd = Discriminator()

# optimizer
optimizerD = Adam(netd.parameters(),lr=opt.lr,betas=(opt.beta1,0.999))
optimizerG = Adam(netg.parameters(),lr=opt.lr,betas=(opt.beta1,0.999))

# criterion
criterion = nn.BCELoss()

fix_noise = Variable(t.FloatTensor(opt.batch_size,opt.nz,1,1).normal_(0,1))
if opt.gpu:
    fix_noise = fix_noise.cuda()
    netd.cuda()
    netg.cuda()
    criterion.cuda()  

import matplotlib.pyplot as plt

# 存储每个迭代的损失
losses_D = []
losses_G = []

for epoch in range(opt.max_epoch):
    for ii, data in enumerate(dataloader, 0):
        real, _ = data
        input = Variable(real)  # 将真实图像包装为PyTorch变量，用于计算图中
        label = Variable(t.ones(input.size(0)))  # 创建与真实图像数量相同的标签变量，所有值为1，表示真实数据
        noise = t.randn(input.size(0), opt.nz, 1, 1)  # 生成与真实图像数量相同的随机噪声，用于生成假图像
        noise = Variable(noise)  # 将随机噪声包装为PyTorch变量，用于计算图中
        
        if opt.gpu:
            noise = noise.cuda()
            input = input.cuda()
            label = label.cuda()
        
        # ----- train netd -----
        netd.zero_grad()
        ## train netd with real img
        output = netd(input)
        error_real = criterion(output.squeeze(), label)
        error_real.backward()
        D_x = output.data.mean()
        ## train netd with fake img
        fake_pic = netg(noise).detach()
        output2 = netd(fake_pic)
        label.data.fill_(0)  # 0 for fake
        error_fake = criterion(output2.squeeze(), label)
        error_fake.backward()
        D_x2 = output2.data.mean()
        error_D = error_real + error_fake
        optimizerD.step()
        
        ##################### ------ train netg using feature matching ------- ##################### 
        ###
        ### you code
        ###
        ############################################################################################ 
    if epoch % 2 == 0:
        fake_u = netg(fix_noise)
        imgs = make_grid(fake_u.data * 0.5 + 0.5).cpu()  # CHW
        img_path = os.path.join(result_dir, f'epoch_{epoch}.png')
        plt.imshow(imgs.permute(1, 2, 0).numpy())  # HWC
        plt.axis('off')  # 不显示坐标轴
        plt.savefig(img_path)
        plt.close()

# 绘制损失图像
plt.figure(figsize=(10, 5))
plt.title("Generator and Discriminator Loss During Training")
plt.plot(losses_G, label="G")
plt.plot(losses_D, label="D")
plt.xlabel("iterations")
plt.ylabel("Loss")
plt.legend()
loss_img_path = os.path.join(result_dir, 'loss_curve.png')
plt.savefig(loss_img_path)
plt.close()
